import java.security.*;
import java.io.FileInputStream;
import java.io.*;
import java.math.BigInteger;
import javax.crypto.*;
import java.util.Arrays;
import java.lang.Math;
import java.io.FileWriter; 

public class Assignment2 {

    public static BigInteger generateProbablePrime(){
        SecureRandom random = new SecureRandom();
        BigInteger prime = BigInteger.probablePrime(512, random);
        return prime;
    }

    public static BigInteger[] getValues(){
        BigInteger p = generateProbablePrime();
        BigInteger q = generateProbablePrime();
        BigInteger n = p.multiply(q);
        BigInteger phi = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
        return new BigInteger[]{n, phi, p, q};
    }

    public static BigInteger[] xgcd(BigInteger a, BigInteger b){
        BigInteger prev;
        BigInteger tmp;
        BigInteger x = BigInteger.ZERO;
        BigInteger y = BigInteger.ONE;
        BigInteger [] result = new BigInteger[] {BigInteger.ONE, BigInteger.ZERO, BigInteger.ZERO};
        while(!b.equals(BigInteger.ZERO)){
            prev = x;
            x = result[0].subtract((a.divide(b)).multiply(x));
            result[0] = prev;

            prev = y;
            y = result[1].subtract((a.divide(b)).multiply(y));
            result[1] = prev;

            tmp = a;
            a = b;
            b = tmp.mod(b);
            result[2] = a;
        }
        return result;
    }

    public static BigInteger multInv(BigInteger a, BigInteger b){
        BigInteger[] xgcd = xgcd(a, b);
        if(xgcd[0].signum() == 1){
            return xgcd[0];
        }
        else{
            return xgcd[0].add(b);
        }
    }

    public static BigInteger decrypt(BigInteger c, BigInteger d, BigInteger p, BigInteger q){
        BigInteger modp = d.mod(p.subtract(BigInteger.ONE));

        System.out.println("modp: " + modp);

        BigInteger modq = d.mod(q.subtract(BigInteger.ONE));

        System.out.println("modq: " + modq);

        BigInteger expp = modularExp(c, modp, p);

        System.out.println("expp: " + expp);

        BigInteger expq = modularExp(c, modq, q);

        System.out.println("expq: " + expq);

        BigInteger multInv = multInv(q, p);

        System.out.println("inv: " + multInv);

        return expq.add(((multInv.multiply(expp.subtract(expq))).mod(p)).multiply(q));
    }

    private static BigInteger modularExp(BigInteger a, BigInteger b, BigInteger p){
        BigInteger y = BigInteger.ONE;
        String bbits = b.toString(2);
        for(int i = 0; i < bbits.length(); i++){
            if(b.testBit(i)){
                y = y.multiply(a).mod(p);
            }
            a = a.multiply(a).mod(p);
        }   
        return y;
    }

    private static byte[] hash(byte[] input) throws NoSuchAlgorithmException{
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        return messageDigest.digest(input);
    }

    public static void main(String[] args){
        BigInteger c = new BigInteger("14");
        BigInteger d = new BigInteger("28");
        BigInteger p = new BigInteger("11");
        BigInteger q = new BigInteger("7");
        BigInteger ans = decrypt(c,d,p,q);
        System.out.println(ans);
        // BigInteger e = new BigInteger("65537");
        // BigInteger[] values = getValues();
        // while(!xgcd(e, values[1])[2].equals(BigInteger.ONE)){
        //     values = getValues();
        // }
        // BigInteger n = values[0];
        // BigInteger phi = values[1];
        // BigInteger p = values[2];
        // BigInteger q = values[3];
        // BigInteger d = multInv(n, phi);
        // try{
        //     File inputFile = new File(args[0]);
        //     FileInputStream inputStream = new FileInputStream(inputFile);
        //     byte[] input = inputStream.readAllBytes();  
        //     inputStream.close();
        //     byte[] hashedInput = hash(input);
        //     BigInteger c = new BigInteger(1, hashedInput);
        //     BigInteger signature = decrypt(p, q, c, d);
            // System.out.println(signature.toString(16));

            // PrintWriter modFile = new PrintWriter("Modulus.txt", "UTF-8");
            // modFile.println(n.toString(16));
            // modFile.close();
        // }
        // catch(Exception ex){
        //     ex.printStackTrace();
        // }
    }
}